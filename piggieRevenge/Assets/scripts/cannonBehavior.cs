﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cannonBehavior : MonoBehaviour {

	public static int cannonMax = 60;
	public static int cannonMin = 0;
	//public KeyCode spaceBar = KeyCode.Space;
	public Vector3 mouseInWorld;
	//public ButtonCode fire = KeyCode.Space;


	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

		mouseInWorld = Camera.main.ScreenToWorldPoint(
			new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10f));

		// direction of cannon facing mouse posistion
		Vector3 direction = mouseInWorld - transform.position;

		// rotates cannon toward mouse
		Quaternion rotation = Quaternion.LookRotation(direction);

		Quaternion checkRot = new Quaternion (0, 0, rotation.z, rotation.w);

		if(checkRot.eulerAngles.z > 0f && checkRot.eulerAngles.z < 70f){
			transform.rotation = new Quaternion(0, 0, rotation.z, rotation.w); 
		}
	}
}
