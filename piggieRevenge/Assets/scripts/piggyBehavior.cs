﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class piggyBehavior : MonoBehaviour {

	public Rigidbody2D piggy;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		Vector3 mouseInWorld = Camera.main.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 10f));
		if (transform.parent != null) {
			Vector3 direction = mouseInWorld - transform.parent.position;
			if (Input.GetMouseButtonDown (0)) {
				GetComponent<Rigidbody2D> ().AddForce (direction * 50);
				piggy.gravityScale = 0.7f;
				piggy.transform.parent = null;
			
			}

		}

		
	}
}
